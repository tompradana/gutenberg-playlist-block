const nodeEnv = process.env.NODE_ENV || 'production';

module.exports = {
    mode: nodeEnv,
    entry: './ds-audio-playlist.js',
    output: {
        path: __dirname,
        filename: 'ds-audio-playlist.build.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader'
            }
        }]
    },
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM'
    }
}
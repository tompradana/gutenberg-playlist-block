// Eksperimental import Sortable from 'gutenberg-sortable';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;
const { TextControl } = wp.components;

registerBlockType( 'ds/audio-playlist', {
	title: __( 'DS Playlist' ),
	description: __( 'Audio Playlist' ),
	category: 'common',
	icon: 'playlist-audio',

	// editor
	edit: ( props ) => {
		const { tracks } = props.attributes;
		const { album } = props.attributes;

		// console.log( tracks );

		return (
			<div className={ props.className }>
				<h3>{ __( "Playlist" ) }</h3>

				<TextControl
					className="album-name"
					style={{ height: 30 }}
					placeholder={ __( 'Album Name' ) }
	                value={ album }
	                onChange={ albumname => {
	                	props.setAttributes({ album: albumname} )
	                }}
	            />

				{/* Eksperimental
					<Sortable
                    className="tracks"
                    items={ tracks }
                    axis="grid"
                   	onSortEnd={ ( tracks ) => {
                   		props.setAttributes({ tracks }); 
                   	}}
                >*/}

                {tracks.map((track,trackIndex) =>
                	<div className="track-item">
	                    <TextControl
	                    	className="track-title"
	                    	style={{ height: 30 }}
							placeholder={ __( 'Title@Artist' ) }
	                    	value={ track.title }
	                    	// autoFocus
	                    	onChange={ title => {
	                    		var newTracks = [];
								const newObject = Object.assign({}, track, {
									title: title
								});
								
								props.attributes.tracks.map((a,i)=>{
										if ( a.index == newObject.index ) {
											newTracks.push(newObject);
										} else {
											newTracks.push(a);
										}
									}
								);

								//console.log('test',newTracks);
								props.setAttributes({ tracks: newTracks }); 
							}}
	                    />
	                    <TextControl
	                    	className="track-url"
	                    	style={{ height: 30 }}
							placeholder={ __( 'Url' ) }
	                    	value={ track.url }
	                    	autoFocus
	                    	onChange={ url => {
	                    		var newTracks = [];
								const newObject = Object.assign({}, track, {
									url: url
								});
								
								props.attributes.tracks.map((a,i)=>{
										if ( a.index == newObject.index ) {
											newTracks.push(newObject);
										} else {
											newTracks.push(a);
										}
									}
								);

								//console.log('test',newTracks);
								props.setAttributes({ tracks: newTracks }); 
							}}
	                    />
	                    <a
							className="removeTrack"
							onClick={() => {
								const newTracks = tracks
									.filter(item => item.index != track.index)
									.map(t => {
										if (t.index > track.index) {
											t.index -= 1;
										}
										return t;
									});
									props.setAttributes({
										tracks: newTracks
									});
								}}
						>{ __( 'Remove' ) }</a>
						<div style={ { clear: "both" } }></div>
	                </div>
                )}

               {/* Eksperimental
               	   </Sortable> */}

				<button
					className="add-new-track button button-primary"
					onClick={content =>
						props.setAttributes({
							tracks: [
								...props.attributes.tracks,
								{
									index: props.attributes.tracks.length,
									title: "",
									url: ""
								}
							]
						})
					}
				>
				{ __( 'Add New Track' ) }
				</button>
			</div>
		);
	},

	// front end
	save: (props) => {
		const { tracks } = props.attributes;

		return null;
	}
});
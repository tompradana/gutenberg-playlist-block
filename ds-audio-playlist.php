<?php
/**
 * Plugin Name: 	DS Audio Playlist
 * Plugin URI: 		https://tommypradana.com
 * Author: 			Tommy Pradana
 * Author URI: 		https://tommypradana.com
 * Version: 		1.0.0
 * Description:		Gutenberg audio playlist
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Enqueue block script
function ds_register_blocks() {
	wp_register_script( 'ds-audio-playlist', 
		plugins_url( 'dist/ds-audio-playlist.build.js', __FILE__ ), 
		array( 'wp-blocks', 'wp-element', 'wp-editor', 'wp-i18n', 'wp-components' ) 
	);

	wp_register_style( 'ds-audio-playlist',
		plugins_url( 'dist/ds-audio-playlist-editor.css', __FILE__ ),
		array( 'wp-edit-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'dist/ds-audio-playlist-editor.css' )
	);

	if ( function_exists( 'register_block_type' ) ) {
		register_block_type( 'ds/audio-playlist', array( 
			'editor_script' 	=> 'ds-audio-playlist',
			'editor_style' 		=> 'ds-audio-playlist',
			'render_callback' 	=> 'ds_block_audio_playlist_render',
			// Server Side Render
			'attributes'		=> array(
				'tracks' => array(
					'type'		=> 'array',
					'default' 	=> array(),
					'items'		=> array(
						'type'	=> 'object'
					)
				),
				'album' => array(
					'type'		=> 'string',
					'default'	=> ''
				)
			),
		) );
	}
}
add_action( 'init', 'ds_register_blocks' );

// Server side render handler
function ds_block_audio_playlist_render( $atts ) {
	static $instance = 0;
	$instance++;

	if ( empty( $atts['tracks'] ) ) {
		return;
	}

	ob_start(); 
	if ( 1 === $instance ) { 
		do_action( 'wp_playlist_scripts', 'audio', 'light' );
	}

	$tracks = array(
		'type' => 'audio',
		'tracklist' => true,
		'tracknumbers' => true,
		'images' => true,
		'artists' => true
	);
	foreach( $atts['tracks'] as $key => $track ) {

		$title = $track['title'];
		$artist = '';
		if ( strpos( $title, '@' ) !== false ) {
			$newtitle = explode('@', $title);
			$title = trim( $newtitle[0] );
			if ( isset( $newtitle[1]) && !empty( $newtitle[1]) ) {
				$artist = trim( $newtitle[1] );
			}
		}

		$tracks['tracks'][] = array(
			'src' 			=> esc_url( $track['url'] ),
			'type' 			=> 'audio/mpeg',
			'title'			=> $title,
			'caption' 		=> '',
			'description' 	=> '',
			'image'			=> array(), //src, width, height
			'thumb'			=> array(), //src, width, height
			'meta' 			=> array(
				'artist'	=> $artist,
				'album'		=> htmlspecialchars( $atts['album'] )
			)
		);
	}
	?>
	<div class="wp-playlist wp-audio-playlist wp-playlist-light">
		<div class="wp-playlist-current-item"></div>
		<audio controls="controls" preload="none" width="100%"></audio>
		<script type="application/json" class="wp-playlist-script">
			<?php echo json_encode( $tracks ); ?>
		</script>
	</div>

	<?php
	return ob_get_clean();
}
